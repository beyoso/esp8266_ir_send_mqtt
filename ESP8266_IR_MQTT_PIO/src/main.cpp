/* IRremoteESP8266: IRsendDemo - demonstrates sending IR codes with IRsend.
 *
 * Version 1.1 January, 2019
 * Based on Ken Shirriff's IrsendDemo Version 0.1 July, 2009,
 * Copyright 2009 Ken Shirriff, http://arcfn.com
 *
 * An IR LED circuit *MUST* be connected to the ESP8266 on a pin
 * as specified by kIrLed below.
 *
 * TL;DR: The IR LED needs to be driven by a transistor for a good result.
 *
 * Suggested circuit:
 *     https://github.com/crankyoldgit/IRremoteESP8266/wiki#ir-sending
 *
 * Common mistakes & tips:
 *   * Don't just connect the IR LED directly to the pin, it won't
 *     have enough current to drive the IR LED effectively.
 *   * Make sure you have the IR LED polarity correct.
 *     See: https://learn.sparkfun.com/tutorials/polarity/diode-and-led-polarity
 *   * Typical digital camera/phones can be used to see if the IR LED is flashed.
 *     Replace the IR LED with a normal LED if you don't have a digital camera
 *     when debugging.
 *   * Avoid using the following pins unless you really know what you are doing:
 *     * Pin 0/D3: Can interfere with the boot/program mode & support circuits.
 *     * Pin 1/TX/TXD0: Any serial transmissions from the ESP8266 will interfere.
 *     * Pin 3/RX/RXD0: Any serial transmissions to the ESP8266 will interfere.
 *   * ESP-01 modules are tricky. We suggest you use a module with more GPIOs
 *     for your first time. e.g. ESP-12 etc.
 */
///////Wifi
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>//V6.03
//////Arduino
#include <Arduino.h>
////Pin Ir

#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <IRac.h> 
#include <IRutils.h> 


#include "DHT.h"
void sendCode(String code);
/////////////Emiter

const uint16_t kIrLed = 4;  // ESP8266 GPIO pin to use. Recommended: 4 (D2).
////////////////////////Recived
IRsend irsend(kIrLed);  // Set the GPIO to be used to sending the message.
const uint16_t kRecvPin = 2;//GPIO WemosD1 mini D4
const uint16_t kCaptureBufferSize = 1024;
const uint8_t kTimeout = 50;
const uint16_t kMinUnknownSize = 12;
IRrecv irrecv(kRecvPin, kCaptureBufferSize, kTimeout, true);
decode_results results; 

//DHT

DHT dht;
float humAc = 0;
float temAc = 0;
float humOld = 0;
float temOld = 0;
int contDht = 0;
int periodoDht = 2000;
unsigned long tiempoDht;
int periodoEnvio = 20000;
unsigned long tiempoEnvio;
//ESP8266
// establecemos los parametros para tener acceso a la red
const char* ssid = "vereda-R";
const char* password = "vereda2019";
const char* mqtt_server = "192.168.1.100";
const int mqtt_port= 1883;

const char* topicTemp = "campo/salon/temperatura";
const char* topicHum = "campo/salon/humedad";
const char* topicLedEmiter = "campo/salon/ledEmiter";
const char* topicLedReceived = "campo/salon/ledReceived";
const char* nameNode = "NodoSalon";
WiFiClient espClient;
PubSubClient client(espClient);
//mode
byte mode = 0;
uint64_t codeOld = 0x8800549;
String code ;
uint16_t kMinUnknownSizeOld = 0;

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

uint64_t getUInt64fromHex(const char * str)
{
    uint64_t accumulator = 0;
    for (size_t i = 0 ; isxdigit((unsigned char)str[i]) ; ++i)
    {
        char c = str[i];
        Serial.println(c);
        accumulator *= 16;
        if (isdigit(c)) /* '0' .. '9'*/
            accumulator += c - '0';
        else if (isupper(c)) /* 'A' .. 'F'*/
            accumulator += c - 'A' + 10;
        else /* 'a' .. 'f'*/
            accumulator += c - 'a' + 10;

    }

    return accumulator;
}

char *uint64_to_string(uint64_t input)
{
    static char result[60] = "";
    // Clear result from any leftover digits from previous function call.
    memset(&result[0], 0, sizeof(result));
    // temp is used as a temporary result storage to prevent sprintf bugs.
    char temp[21] = "";
    char c;
    uint8_t base = 10;

    while (input) 
    {
        int num = input % base;
        input /= base;
        c = '0' + num;

        sprintf(temp, "%c%s", c, result);
        strcpy(result, temp);
    } 
    return result;
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(nameNode)) {
      Serial.println("connected");
      // Enviamos los datos obtenidos
     client.publish(topicLedEmiter, nameNode);
     client.publish(topicHum, nameNode);
     client.publish(topicTemp, nameNode);
      // ... and resubscribe
      client.subscribe(topicLedEmiter);
      client.subscribe(topicLedReceived);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
String getCode (){
    //String res = "";
    String res ="";
    uint32_t now = millis();
    //while(1){ 
      if(irrecv.decode(&results))
      {
        yield();   
        Serial.print( "value: ");
        Serial.println(uint64_to_string(results.value));
        Serial.print(" comand: ");
        Serial.println(results.command);
        Serial.print("decode type: ");
        Serial.println(results.decode_type);
        Serial.print("resultToHexidecimal: ");
        Serial.println( resultToHexidecimal(&results));
        //Serial.print("String.length: ");
        //Serial.println(resultToHexidecimal(&results));
        Serial.print("bits: ");
        Serial.println(results.bits);
        Serial.print("rawlen: ");
        Serial.println(results.rawlen);
        //Serial.print("resultToHumanReadableBasic: ");
        //Serial.println(resultToHumanReadableBasic(&results));
                  // Feed the WDT (again)
        //res = uint64_to_string(results.value);
        //res += "-";
        //res += uint64_to_string(results.command);
        String aux = uint64_to_string(results.value);
        sendCode(aux);
        return res = uint64_to_string(results.value);
        //return res = resultToSourceCode(&results);
      }  
        yield(); 
    //}
    return res;
}
void setTempYHumedad()
{
  
  delay(dht.getMinimumSamplingPeriod());
  float h = dht.getHumidity();
  float t = dht.getTemperature();
  if(!(isnan(h)|| isnan(t)))
  {
    humAc += h;
    temAc += t;
    contDht++;
  }
  
  Serial.print("temperature: ");Serial.print(t);Serial.print(" humedad: ");Serial.println(h); 
  
}
void sendHumAndTemp(float hum, float temp)
{
    String payload ;
    payload ="{";payload += "\"temperature\":"; payload += temp; payload += "}";
    char attributes[200];
  
    payload.toCharArray( attributes, 200 );
    Serial.println( attributes );
        if (client.publish( topicTemp, attributes ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
    payload  ="{";payload += "\"humidity\":"; payload += hum; payload += "}";
    char attributes2[200];
  
    payload.toCharArray( attributes2, 200 );
    Serial.println( attributes2 );
        if (client.publish( topicHum, attributes2 ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
  
}
void sendCode(String code)
{
    String payload ;
    payload ="{";payload += "\"code\":"; payload += code; payload += "}";
    char attributes[200];
    payload.toCharArray( attributes, 400 );
    Serial.println( attributes );
        if (client.publish( topicLedReceived, attributes ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
    
}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message recibido [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  //JSON
  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, payload);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  mode = doc["mode"];

  if(mode == 1){
      uint64_t code2 = doc["code"];
   
    Serial.print("\nCODE :\n");
    Serial.println(uint64_to_string(code2));
    //Serial.print("\nLeng :\n");
    //Serial.println(code2.length;
    //Serial.print("\nSend code:\n");
    //String res = ;
    
    //Serial.println(uint64_to_string(((int64_t)code));
  irrecv.disableIRIn();
    irsend.sendLG(code2,(uint16_t)28);
    irrecv.enableIRIn();
    return;
  }
  else if(mode==2){
    return;
    //getCode();
  }else if(mode==3){
    irsend.sendLG(codeOld,28);   
  }
  else{mode = 0;}
  
}
void setup() 
{
  //Serial
  Serial.begin(115200);
  //Received led init
  irsend.begin();
  irrecv.setUnknownThreshold(kMinUnknownSize);
  irrecv.enableIRIn();  // Start the receiver
  //DHT 
  dht.setup(D3,dht.DHT22);
  tiempoDht = millis();
  tiempoEnvio = millis();
  //Wifi
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  
}

void loop() 
{
  if(!WiFi.isConnected()){setup_wifi();}
  if(!client.connected()) {reconnect();}
  client.loop();
  getCode ();
  /*if(kMinUnknownSize !=kMinUnknownSize)
  {
    kMinUnknownSizeOld = kMinUnknownSize ;
    Serial.println(kMinUnknownSize);
  }
  /*
  if(mode == 2)
  {   
    String code = getCode();
    yield();
    if(code.length()>1)
    {
      sendCode(code);
      mode = 0;
    }
  }
  */
  if(mode == 1){

    //irsend.sendLG(code,28);
    //irsend.send();
    mode = 0;
    return;
  }
  
  ///Actualizamos temperatura
  if(millis()>tiempoDht +periodoDht)
  {
    tiempoDht = millis();
    setTempYHumedad();
  }
  ///Enviamos media temperatura
  if(millis()>tiempoEnvio+periodoEnvio)
  {
    tiempoEnvio = millis();
    

    humAc = (humAc/contDht);
    temAc = (temAc/contDht);
      if(temAc != temOld || humAc != humOld )
      {
        temOld = temAc;
        humOld = humAc; 
        sendHumAndTemp(humAc,temAc);
      }
      humAc = 0;
      temAc =0;
      contDht = 0;
  }
}



